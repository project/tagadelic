<?php

namespace Drupal\tagadelic;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;

/**
 * Class TagadelicCloud.
 *
 * @package Drupal\tagadelic
 */
class TagadelicCloudTaxonomy extends TagadelicCloudBase {

  /**
   * Returns the @config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Returns the database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactory $config_factory, Connection $database) {
    parent::__construct();
    $this->configFactory = $config_factory;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function createTags(array $options = []) {
    $config = $this->configFactory->getEditable('tagadelic.settings');
    $vocabularies = (array) $config->get('tagadelic_vocabularies');
    $max_amount = 50;

    $query = $this->database->select('taxonomy_index', 'i');
    $alias = $query->leftjoin('taxonomy_term_field_data', 't', '%alias.tid = i.tid');
    $query->addExpression('COUNT(i.nid)', 'count');
    $query->addField($alias, 'tid');
    $query->addField($alias, 'name');
    $query->addField($alias, 'description__value');
    $query->orderBy('count', 'DESC');

    // If no vocabularies have been configured use them all.
    if (count($vocabularies)) {
      foreach ($vocabularies as $key => $value) {
        if ($key !== $value) {
          $query->condition('t.vid', $key, '<>');
        }
      }
    }

    $query->range(0, $max_amount)
      ->groupBy('t.tid')
      ->groupBy('t.name')
      ->groupBy('t.description__value');

    foreach ($query->execute() as $item) {
      $tag = new TagadelicTag($item->tid, $item->name, $item->count);
      $this->addTag($tag);
    }
  }

}
