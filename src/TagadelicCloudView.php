<?php

namespace Drupal\tagadelic;

use Drupal\views\ViewExecutable;

/**
 * Class TagadelicCloudView. Provides base class for view tags.
 *
 * @package Drupal\tagadelic
 */
class TagadelicCloudView extends TagadelicCloudBase {

  /**
   * View object.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected $view;

  /**
   * The field on each ResultRow object that will be used to create a tag.
   *
   * @var string
   */
  protected $count_field;

  /**
   * The field on each ResultRow object that will be used to create a tag.
   *
   * @var int
   */
  protected $override_sort = 0;

  /**
   * Helper to set the view.
   */
  public function setView(ViewExecutable $view) {
    $this->view = $view;
    return $this;
  }

  /**
   * Helper to override sort.
   */
  public function setOverrideSort($override_sort) {
    $this->override_sort = $override_sort;
    return $this;
  }

  /**
   * Helper to set the count.
   */
  public function setCountField($count_field) {
    $this->count_field = $count_field;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createTags(array $options = []) {
    if (empty($options['view']) || empty($options['count_field'])) {
      return;
    }

    $this->setView($options['view']);
    $this->setCountField($options['count_field']);

    if (!empty($options['override_sort'])) {
      $this->setOverrideSort($options['override_sort']);
    }

    // The field for the count_field may be aliased
    // We may have a Drupal field such as comment count on node as the count
    // field. First check to see if there is an alias in the query's fields.
    foreach ($this->view->build_info['query']->getFields() as $id => $field) {
      if ($field['field'] == $this->count_field) {
        $count_field_alias = $id;
        break;
      }
    }

    // If there is no alias in the fields there may be a field that uses a count
    // in the aggregation settings. We therefore need to check the expressions.
    if (!isset($count_field_alias)) {
      foreach ($this->view->build_info['query']->getExpressions() as $id => $expression) {
        if (strpos($expression['expression'], 'COUNT') !== FALSE && strpos($expression['expression'], $this->count_field) !== FALSE) {
          $count_field_alias = $id;
        }
      }
    }

    foreach ($this->view->result as $id => $row) {
      // As the tags are not going to be used in the markup
      // we can pass in dummy names and ids.
      $name = md5(time() . $id);
      $count_field = $this->count_field;

      // If we there is no alias for the field assume
      // it is present on the Resultrow object.
      $prop = $count_field_alias ?? $count_field;
      if (isset($row->{$prop})) {
        $tag = new TagadelicTag($id + 1, $name, $row->{$prop});
        $this->addTag($tag);
      }
    }
  }

  /**
   * Helper to compare values.
   */
  protected function cmp($a, $b) {
    return strcmp($b->getCount(), $a->getCount());
  }

}
