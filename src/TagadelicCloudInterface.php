<?php

namespace Drupal\tagadelic;

/**
 * Provides an interface for returning tags for a tag cloud.
 */
interface TagadelicCloudInterface {

  /**
   * Return an array of Tagadelic Tags.
   *
   * @return array
   *   An array of tags.
   */
  public function getTags();

  /**
   * Add a TagadelicTag object to the tags array.
   *
   * @param \Drupal\tagadelic\TagadelicTag $tag
   *   The tag being added.
   */
  public function addTag(TagadelicTag $tag);

}
