<?php

namespace Drupal\tagadelic;

use Drupal\Component\Utility\Html;

/**
 * Class TagadelicTag. Provides base class for tags.
 */
class TagadelicTag {

  /**
   * Identifier of this tag.
   *
   * @var int
   */
  private $id = 0;

  /**
   * A human readable name for this tag.
   *
   * @var string
   */
  private $name = '';

  /**
   * A human readable piece of HTML-formatted text.
   *
   * @var string
   */
  private $description = '';

  /**
   * Absolute count for the weight.
   *
   * @var float
   */
  private $count = 0.0000001;

  /**
   * Weight, i.e. tag-size will be extracted from this.
   *
   * @var float
   */
  private $weight = 0.0;

  /**
   * Initialize this tag.
   */
  public function __construct($id, $name, $count) {
    $this->id   = $id;
    $this->name = $name;
    if ($count != 0) {
      $this->count = $count;
    }
  }

  /**
   * Getter for the ID.
   *
   * @return int
   *   Identifier.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Getter for the name.
   *
   * @return string
   *   The human readable name.
   */
  public function getName() {
    return Html::escape($this->name);
  }

  /**
   * Getter for the description.
   *
   * @return string
   *   The human readable description.
   */
  public function getDescription() {
    return Html::escape($this->description);
  }

  /**
   * Returns the weight, getter only.
   *
   * @return float
   *   The weight of this tag.
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * Returns the count, getter only.
   *
   * @return int
   *   The count as provided when Initializing the Object.
   */
  public function getCount() {
    return $this->count;
  }

  /**
   * Sets the optional description.
   *
   * @param string $description
   *   String a description.
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Setter for weight.
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * Calculates a more evenly distributed value.
   */
  public function distributed() {
    return log($this->count);
  }

}
