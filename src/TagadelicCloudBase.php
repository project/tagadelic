<?php

namespace Drupal\tagadelic;

/**
 * Defines a base TagadelicCloud implementation.
 */
abstract class TagadelicCloudBase implements TagadelicCloudInterface {

  /**
   * An array of TagadelicTag objects.
   *
   * @var array
   */
  protected $tags;

  /**
   * Amount of steps to weight the cloud in. Defaults to 6.
   *
   * @var int
   */
  private $steps = 6;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->tags = [];
  }

  /**
   * Populate the member array of TagadelicTag objects.
   */
  abstract public function createTags(array $options = []);

  /**
   * {@inheritdoc}
   */
  public function getTags(array $options = []) {
    $this->resetTags();
    $this->createTags($options);
    $this->recalculate();
    return $this->tags;
  }

  /**
   * {@inheritdoc}
   */
  public function addTag(TagadelicTag $tag) {
    $this->tags[] = $tag;
    return $this;
  }

  /**
   * Calculates the weights on the tags.
   */
  protected function recalculate() {
    $tags = [];
    // Find minimum and maximum log-count.
    $min = 1e9;
    $max = -1e9;
    foreach ($this->tags as $id => $tag) {
      $min = min($min, $tag->distributed());
      $max = max($max, $tag->distributed());
      $tags[$id] = $tag;
    }
    // Note: we need to ensure the range is slightly too large to make sure even
    // the largest element is rounded down.
    $range = max(.01, $max - $min) * 1.0001;
    foreach ($tags as $id => $tag) {
      $this->tags[$id]->setWeight(1 + floor($this->steps * ($tag->distributed() - $min) / $range));
    }
    return $this;
  }

  /**
   * Helper to sort tags by type.
   */
  protected function sort($by_property) {
    if ($by_property == 'random') {
      shuffle($this->tags);
    }
    else {
      @usort($this->tags, [$this, "sortBy{$by_property}"]);
    }

    return $this;
  }

  /**
   * Helper to sort tags by name.
   */
  private function sortByname($a, $b) {
    return strcoll($a->getName(), $b->getName());
  }

  /**
   * Helper to sort tags by count.
   */
  private function sortBycount($a, $b) {
    $ac = $a->getCount();
    $bc = $b->getCount();

    if ($ac == $bc) {
      return 0;
    }

    // Highest first, High to low.
    return ($ac < $bc) ? +1 : -1;
  }

  /**
   * Helper to reset tags.
   */
  private function resetTags() {
    $this->tags = [];
  }

}
