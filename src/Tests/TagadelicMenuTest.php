<?php

namespace Drupal\tagadelic\Tests;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for displaying tagadelic page.
 *
 * @group tagadelic
 */
class TagadelicMenuTest extends BrowserTestBase {

  /**
   * A user with permission to access the administrative toolbar.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'menu_ui', 'user', 'taxonomy', 'toolbar', 'tagadelic'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an administrative user and log it in.
    $this->adminUser = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($this->adminUser);

    // Assert that the toolbar is present in the HTML.
    $this->assertSession()->responseContains('id="toolbar-administration"');
  }

  /**
   * Test page displays.
   */
  public function testTagadelicMenusExist() {
    $this->drupalGet('admin/structure');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkByHrefExists('admin/structure/tagadelic');
  }

}
