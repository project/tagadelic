<?php

namespace Drupal\tagadelic\Tests;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for tagadelic service.
 *
 * @group tagadelic
 */
class TagadelicServiceTest extends BrowserTestBase {

  /**
   * The vocabulary used for creating terms.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $vocabulary;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tagadelic', 'taxonomy', 'node'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create an article content type.
    $this->drupalCreateContentType([
      'type' => 'article',
    ]);

    // Create the vocabulary for the tag field.
    $this->vocabulary = Vocabulary::create([
      'name' => 'Views testing tags',
      'vid' => 'views_testing_tags',
    ]);
    $this->vocabulary->save();

    $field_name = 'field_' . $this->vocabulary->id();
    $handler_settings = [
      'target_bundles' => [
        $this->vocabulary->id() => $this->vocabulary->id(),
      ],
      'auto_create' => TRUE,
    ];

    // Create the tag field.
    if (!FieldStorageConfig::loadByName('node', $field_name)) {
      FieldStorageConfig::create([
        'field_name' => $field_name,
        'type' => 'entity_reference',
        'entity_type' => 'node',
        'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
        'settings' => [
          'target_type' => 'taxonomy_term',
        ],
      ])->save();
    }

    if (!FieldConfig::loadByName('node', 'article', $field_name)) {
      $handler_settings = [
        'target_bundles' => [
          $this->vocabulary->id() => $this->vocabulary->id(),
        ],
        'auto_create' => TRUE,
      ];
      FieldConfig::create([
        'field_name' => $field_name,
        'entity_type' => 'node',
        'bundle' => 'article',
        'label' => 'Tags',
        'settings' => [
          'handler' => 'default',
          'handler_settings' => $handler_settings,
        ],
      ])->save();
    }

    \Drupal::service('entity_display.repository')->getFormDisplay('node', 'article', 'default')
      ->setComponent($field_name, [
        'type' => 'entity_reference_autocomplete_tags',
        'weight' => -4,
      ])
      ->save();

    \Drupal::service('entity_display.repository')->getViewDisplay('node', 'article', 'default')
      ->setComponent($field_name, [
        'type' => 'entity_reference_label',
        'weight' => 10,
      ])
      ->save();

    \Drupal::service('entity_display.repository')->getViewDisplay('node', 'article', 'teaser')
      ->setComponent($field_name, [
        'type' => 'entity_reference_label',
        'weight' => 10,
      ])
      ->save();
  }

  /**
   * Test block placement.
   */
  public function testTagadelicService() {
    $user = $this->drupalCreateUser(['administer taxonomy', 'bypass node access']);
    $this->drupalLogin($user);

    // Create 4 taxonomy terms and and 4 nodes
    // Add various combinations of their terms' ids to the
    // entity reference field on the nodes.
    $term1 = $this->createTerm($this->vocabulary);
    $term2 = $this->createTerm($this->vocabulary);
    $term3 = $this->createTerm($this->vocabulary);
    $term4 = $this->createTerm($this->vocabulary);

    $node = [];
    $node['type'] = 'article';
    $node['field_views_testing_tags'][]['target_id'] = $term1->id();
    $node['field_views_testing_tags'][]['target_id'] = $term2->id();
    $node['field_views_testing_tags'][]['target_id'] = $term3->id();
    $node['field_views_testing_tags'][]['target_id'] = $term4->id();
    $this->drupalCreateNode($node);

    $node['field_views_testing_tags'][] = [];
    $node['field_views_testing_tags'][]['target_id'] = $term1->id();
    $node['field_views_testing_tags'][]['target_id'] = $term2->id();
    $node['field_views_testing_tags'][]['target_id'] = $term3->id();
    $this->drupalCreateNode($node);

    $node['field_views_testing_tags'][] = [];
    $node['field_views_testing_tags'][]['target_id'] = $term1->id();
    $node['field_views_testing_tags'][]['target_id'] = $term2->id();
    $node['field_views_testing_tags'][]['target_id'] = $term3->id();
    $this->drupalCreateNode($node);

    $node['field_views_testing_tags'][] = [];
    $node['field_views_testing_tags'][]['target_id'] = $term1->id();
    $node['field_views_testing_tags'][]['target_id'] = $term2->id();
    $node['field_views_testing_tags'][]['target_id'] = $term3->id();
    $this->drupalCreateNode($node);

    // Create the service and check that it is returning tags.
    $service = \Drupal::service('tagadelic.tagadelic_taxonomy');
    $tags = $service->getTags();
    $this->assertTrue(count($tags) > 0);
    $this->assertEquals(4, count($tags));
    $tag = array_pop($tags);
    $this->assertEquals('Drupal\tagadelic\TagadelicTag', get_class($tag));
  }

  /**
   * Creates and returns a taxonomy term.
   *
   * @param object $vocabulary
   *   The vocabulary for the returned taxonomy term.
   *
   * @return \Drupal\taxonomy\Entity\Term
   *   The created taxonomy term.
   */
  public function createTerm($vocabulary) {
    $filter_formats = filter_formats();
    $format = array_pop($filter_formats);
    $term = Term::create([
      'name' => $this->randomMachineName(),
      'description' => [
        'value' => $this->randomMachineName(),
        // Use the first available text format.
        'format' => $format->id(),
      ],
      'vid' => $vocabulary->id(),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $term->save();
    return $term;
  }

}
