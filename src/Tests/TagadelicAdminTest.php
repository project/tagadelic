<?php

namespace Drupal\tagadelic\Tests;

use Drupal\Core\Language\LanguageInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for displaying tagadelic page.
 *
 * @group tagadelic
 */
class TagadelicAdminTest extends BrowserTestBase {

  /**
   * A user with permission to access the administrative toolbar.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A n array of vocabularies.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $vocabularies;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'menu_ui', 'user', 'taxonomy', 'toolbar', 'tagadelic'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an administrative user and log it in.
    $this->adminUser = $this->drupalCreateUser([], NULL, TRUE);

    $this->drupalLogin($this->adminUser);

    $this->vocabularies = [];

    $vocabulary1 = Vocabulary::create([
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
      'vid' => mb_strtolower($this->randomMachineName()),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => mt_rand(0, 10),
    ]);
    $vocabulary1->save();
    $this->vocabularies[] = $vocabulary1;

    $vocabulary2 = Vocabulary::create([
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
      'vid' => mb_strtolower($this->randomMachineName()),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => mt_rand(0, 10),
    ]);
    $vocabulary2->save();
    $this->vocabularies[] = $vocabulary2;
  }

  /**
   * Test all vocabularies appear on admin page.
   */
  public function testAllVocabulariesLoaded() {
    $this->drupalGet('admin/structure/tagadelic');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('Tag Cloud');

    foreach ($this->vocabularies as $vocabulary) {
      $this->assertSession()->responseContains($vocabulary->get('name'));
    }
  }

}
