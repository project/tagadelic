<?php

namespace Drupal\tagadelic\Tests;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for displaying tagadelic page.
 *
 * @group tagadelic
 */
class TagadelicPageTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tagadelic', 'taxonomy'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test page displays.
   */
  public function testTagadelicPageExists() {
    $user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($user);

    $this->drupalGet('tagadelic');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('Tag Cloud');
  }

}
