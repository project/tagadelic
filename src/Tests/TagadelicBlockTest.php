<?php

namespace Drupal\tagadelic\Tests;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for displaying tagadelic block.
 *
 * @group tagadelic
 */
class TagadelicBlockTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tagadelic', 'taxonomy', 'block'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test block placement.
   */
  public function testTagadelicBlock() {
    // Create user.
    $web_user = $this->drupalCreateUser(['administer blocks']);
    // Login the admin user.
    $this->drupalLogin($web_user);

    $theme_name = \Drupal::config('system.theme')->get('default');

    // Verify the block is listed to be added.
    $this->drupalGet('/admin/structure/block/library/' . $theme_name);

    $this->assertSession()->responseContains($this->t('Tagadelic tag cloud'), 'Block label found.');

    $settings = [
      'label' => $this->t('Tagadelic tag cloud test'),
      'id' => 'tagadelic_block',
      'theme' => $theme_name,
      'num_tags_block' => 5,
    ];
    $this->drupalPlaceBlock('tagadelic_block', $settings);

    // Verify that block is there.
    $this->drupalGet('');
    $this->assertSession()->responseContains($settings['label'], 'Block found.');
  }

}
