# Tagadelic

Tagadelic provides an API and a few simple turnkey modules, which allows you to easily create tagclouds, weighted lists, search-clouds and such.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/tagadelic).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/tagadelic).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Configure permissions:
Home >> Administration >> People
(/admin/people/permissions/module/tagadelic)

- Configure vocabularies used in Tag Cloud:
Home >> Administration >> Structure
(/admin/structure/tagadelic)

- Now you can add Tag Cloud blocks:
Home >> Administration >> Structure
(/admin/structure/block)

- Or you can view the 'tag cloud' of taxonomy tags on the /tagadelic page.


## Maintainers

- Bèr Kessels - [bèr-kessels](https://www.drupal.org/u/b%C3%A8r-kessels)
- Luca Capra - [muka](https://www.drupal.org/u/muka)
- Diego Tejera - [di3gopa](https://www.drupal.org/u/di3gopa)
- Sean T. Walsh - [seantwalsh](https://www.drupal.org/u/seantwalsh)
- Mike Garthwaite - [systemick](https://www.drupal.org/u/systemick)
- Rob Loach - [AstonVictor](https://www.drupal.org/u/robloach)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
