<?php

/**
 * @file
 * Contains tagadelic.theme.inc.
 */

/**
 * Prepares variables for tagadelic view template.
 *
 * Default template: tagadelic-view-tagdelic-list.html.twig.
 *
 * @param array $variables
 *   An associative array.
 */
function template_preprocess_tagadelic_view_tagadelic_list(&$variables) {
  $view = $variables['view'];
  $rows = $variables['rows'];
  $style = $view->style_plugin;
  $options = $style->options;

  $count_field = $options['count_field'];
  if (empty($count_field)) {
    \Drupal::messenger()->addError(t("Please set which field you are using for counting results in the tagadelic display."));
    return '';
  }

  $params = ['view' => $view, 'count_field' => $count_field, 'override_sort' => $options['override_sort']];
  $cloud = \Drupal::service('tagadelic.tagadelic_view');
  $tags = $cloud->getTags($params);

  $variables['default_row_class'] = !empty($options['default_row_class']);
  foreach ($rows as $id => $row) {
    $variables['rows'][$id] = [];
    $variables['rows'][$id]['content'] = $row;
    $variables['rows'][$id]['weight'] = $tags[$id]->getWeight();
  }
}
